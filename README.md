## Tic Tac Toe Game

This project allows users to play a classic game of tic tac toe

By clicking in between the lines of the boxes users will be able to place an X or a O. 
Once a user gets 3 consecutive symbols in a row an alert should pop up telling them that they have won

---

## Install and Run

To obtain the source code to this porject clone the repository from this site
Users will need to download Visual Studio 2019 to run the program 
Download the offical software here: https://visualstudio.microsoft.com/downloads/

Once Visual Studio 2019 opens select the folder containg the repo you just cloned. 
At the top of the application after everything has loaded there should be a start button at the top of the scree. 
pressin this button should run the porgram on your desktop/


## License & Copyright 

Licensed under the GNU General Public License v3.0 

I Chose this type of license because I give any persons authorization to use this code.